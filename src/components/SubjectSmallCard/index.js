import React from "react";
import PropTypes from "prop-types";
import { Paper, Stack, Typography } from "@mui/material";
import PersonIcon from "@mui/icons-material/Person";
import AccessTimeIcon from "@mui/icons-material/AccessTime";

function SubjectSmallCard({ subject }) {
  const style = {
    icon: {
      marginRight: 1,
    },
  };

  return (
    <Paper elevation={2} sx={{ p: 1, width: 220 }}>
      <Stack spacing={1}>
        <Typography variant="h5" sx={{ pl: 1 }}>
          {subject.name}
        </Typography>
        <Stack direction="row">
          <AccessTimeIcon sx={style.icon} />
          <Typography>{subject.schedule}</Typography>
        </Stack>
        <Stack direction="row">
          <PersonIcon sx={style.icon} />
          <Typography>{subject.trainer}</Typography>
        </Stack>
      </Stack>
    </Paper>
  );
}

SubjectSmallCard.propTypes = {
  subject: PropTypes.shape({
    name: PropTypes.string,
    daysOfWeek: PropTypes.string,
    trainer: PropTypes.string,
    schedule: PropTypes.string,
    startDate: PropTypes.string,
    image: PropTypes.string,
  }).isRequired,
};

export default SubjectSmallCard;
