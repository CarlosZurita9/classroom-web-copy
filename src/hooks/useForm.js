import { useState, useEffect } from "react";

function useForm(initialValues, callback, validate, validations) {
  const [values, setValues] = useState(initialValues);

  const [errors, setErrors] = useState({});
  const [isSubmitting, setIsSubmitting] = useState(false);

  const handleChange = (event) => {
    const { name, value, files, type } = event.target;
    if (type === "file") {
      setValues({
        ...values,
        [name]: files,
      });
    } else {
      setValues({
        ...values,
        [name]: value,
      });
    }
  };

  const handleRadioChange = (event) => {
    const { name, value, checked } = event.target;
    if (checked) {
      setValues({
        ...values,
        [name]: values[name] + Number(value),
      });
    } else {
      setValues({
        ...values,
        [name]: values[name] - Number(value),
      });
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    setErrors(validate(values, validations));
    setIsSubmitting(true);
  };

  useEffect(() => {
    if (Object.keys(errors).length === 0 && isSubmitting) {
      callback(values);
    }
    // eslint-disable-next-line
  }, [errors]);
  return { handleChange, handleSubmit, handleRadioChange, values, errors };
}

export default useForm;
