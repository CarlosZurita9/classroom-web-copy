import getDateString from "../helper/getDateString";

const createStudentListAdapter = (data, countries) => {
  const studentList = [];

  function getCountry(countryId) {
    for (let i = 0; i < countries.length; i += 1) {
      if (countries[i].id === countryId) {
        return countries[i].name;
      }
    }
    return "Bolivia";
  }

  if (data !== undefined) {
    for (let i = 0; i < data.length; i += 1) {
      studentList.push({
        id: data[i].id,
        name: `${data[i].firstName} ${data[i].lastName}`,
        email: data[i].email,
        birthday: getDateString(data[i].birthDay),
        image: data[i].image,
        courseId: data[i].courseId,
        country: getCountry(data[i].countryId),
        countryId: data[i].countryId,
        status: data[i].courseId ? "Registered" : "Unregistered",
      });
    }
  }

  return studentList;
};

export default createStudentListAdapter;
