const addSubjectFormInputs = {
  title: "Add Subject Form",
  sections: [
    {
      id: 1,
      title: "Subject data:",
      inputs: [
        {
          id: 1,
          placeholder: "Enter subject name",
          label: "Subject Name",
          value: "",
          required: true,
          type: "text",
          xs: 12,
          sm: 6,
          md: 3,
          validation: {
            isRequired: true,
            test: () => {
              return false;
            },
            message: "",
          },
        },
        {
          id: 2,
          placeholder: null,
          label: "Start Date",
          value: "",
          required: true,
          type: "date",
          xs: 12,
          sm: 6,
          md: 3,
          validation: {
            isRequired: true,
            test: () => {
              return false;
            },
            message: "",
          },
        },
        {
          id: 3,
          placeholder: "Enter Trainer Name",
          label: "Trainer Name",
          value: "",
          required: true,
          type: "text",
          xs: 12,
          sm: 6,
          md: 3,
          validation: {
            isRequired: true,
            test: () => {
              return false;
            },
            message: "",
          },
        },
        {
          id: 4,
          placeholder: null,
          label: "Subject Image",
          value: "",
          required: true,
          type: "file",
          xs: 12,
          sm: 6,
          md: 3,
          validation: {
            isRequired: true,
            test: () => {
              return false;
            },
            message: "",
          },
        },
      ],
    },
    {
      id: 2,
      title: "Schedule:",
      inputs: [
        {
          id: 1,
          placeholder: null,
          label: "Start Hour",
          value: "",
          required: true,
          type: "time",
          xs: 12,
          sm: 12,
          md: 2,
          validation: {
            isRequired: true,
            test: () => {
              return false;
            },
            message: "",
          },
        },
        {
          id: 2,
          placeholder: null,
          label: "End Hour",
          value: "",
          required: true,
          type: "time",
          xs: 12,
          sm: 12,
          md: 2,
          validation: {
            isRequired: true,
            test: (values) => {
              const EndHour = values["End Hour"];
              const StartHour = values["Start Hour"];
              return EndHour <= StartHour;
            },
            message: "End hour must be after the start hour",
          },
        },
      ],
    },
  ],
  radios: [
    {
      id: 1,
      title: "Days",
      values: [
        { id: 1, value: 1, label: "Monday" },
        { id: 2, value: 2, label: "Tuesday" },
        { id: 3, value: 4, label: "Wednesday" },
        { id: 4, value: 8, label: "Thursday" },
        { id: 5, value: 16, label: "Friday" },
      ],
      validation: {
        isRequired: false,
        test: ({ Days }) => {
          return Days === 0;
        },
        message: "At least one checkbox must be selected",
      },
    },
  ],
};

export default addSubjectFormInputs;
