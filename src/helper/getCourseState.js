const getCourseState = (startDate, endDate) => {
  const startingDate = startDate.slice(0, 10);
  const endingDate = endDate.slice(0, 10);
  const currentDate = new Date().toISOString().slice(0, 10);
  if (currentDate < startingDate) return "Inactive";
  if (currentDate > endingDate) return "Finished";
  return "Active";
};

export default getCourseState;
