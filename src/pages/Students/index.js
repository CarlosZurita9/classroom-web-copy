import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Box, Button } from "@mui/material";

import useFetchAndLoad from "../../hooks/useFetchAndLoad";
import createStudentListAdapter from "../../adapter/student.adapter";
import { getStudents, postStudent } from "../../services/student.service";
import { getCountries } from "../../services/country.service";
import Form from "../../components/Form";
import studentFormInputs from "../../components/Form/FormInputs/addStudentFormInputs";
import createImage from "../../services/image.service";
import ErrorCard from "../../components/ErrorCard";
import { API_SCHEME } from "../../constants/api.constants";
import { StudentContextProvider } from "../../contexts/studentContext";
import FilterStudentList from "../../components/FilterStudentList";
import addFilterInputs from "../../components/FilterStudentList/FilterInputs/addFilterInputs";
import SeparatorBar from "../../components/SeparatorBar";
import { fetchStudentsInit } from "../../redux/reducer/students";

function Students() {
  const { callEndpoint } = useFetchAndLoad();
  const [toggleForm, setToggleForm] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [students, setStudents] = useState([]);
  const [countries, setCountries] = useState([]);
  const dispatch = useDispatch();
  const handleToggle = () => {
    setToggleForm((c) => !c);
    setErrorMessage("");
  };

  const style = {
    primary: {
      fontSize: "16px",
      fontWeight: "bolder",
    },
    secundary: {
      fontSize: "16px",
      fontWeight: "bolder",
      background: "grey",
    },
  };

  useEffect(() => {
    let studentsData = [];
    let countryList = [];
    const fetchCountry = async () => {
      try {
        countryList = await callEndpoint(getCountries());
        return countryList.data;
      } catch (error) {
        return null;
      }
    };

    const fetchStudents = async () => {
      let studentList;
      try {
        studentList = await callEndpoint(getStudents());
        studentsData = createStudentListAdapter(
          studentList.data,
          countryList.data,
        );
      } catch (error) {
        studentsData = [];
      }
    };

    const fetch = async () => {
      await fetchCountry();
      await fetchStudents();
      setCountries(countryList.data);
      setStudents(studentsData);
    };

    fetch();
    // eslint-disable-next-line
  }, []);

  const getStudentImageNameFormat = (fName, lName) => {
    const date = new Date();
    const month = date.getMonth() + 1;
    return `${date.getFullYear()}${month}${date.getDate()}${date.getHours()}${date.getMinutes()}_${fName}${lName}`;
  };

  const handleSubmit = async (values) => {
    try {
      const firstName = values["First Name"];
      const lastName = values["Last Name"];
      let image = values["Student Image"][0];
      const formData = new FormData();
      formData.append("files", image, image.name);
      const imageName = getStudentImageNameFormat(firstName, lastName);
      image = await callEndpoint(createImage("student", imageName, formData));
      const body = {
        firstName,
        lastName,
        email: values.Email,
        birthDay: values.Birthday,
        image: `${API_SCHEME}${image.data}`,
        countryId: values.Country,
      };

      const newStudent = await callEndpoint(postStudent(body));
      const studentData = createStudentListAdapter(
        [newStudent.data],
        countries,
      );
      setStudents([...students, ...studentData]);
      dispatch(fetchStudentsInit());

      setToggleForm(false);
      setErrorMessage("");
    } catch (error) {
      if (error.response.data.length > 0) {
        setErrorMessage(`${error.response.data}`);
      } else {
        setErrorMessage(`${error.response.statusText}`);
      }
    }
  };

  return (
    <StudentContextProvider value={[countries, setCountries]}>
      <Box>
        {errorMessage && <ErrorCard message={errorMessage} />}
        {toggleForm && (
          <Form inputs={studentFormInputs(countries)} onSubmit={handleSubmit} />
        )}
        <SeparatorBar title="Students">
          <Button
            variant="contained"
            style={toggleForm ? style.secundary : style.primary}
            onClick={handleToggle}
          >
            Add Student
          </Button>
        </SeparatorBar>
        <Box style={{ padding: "1em 1em" }}>
          <FilterStudentList
            inputs={addFilterInputs(countries)}
            originalList={students}
          />
        </Box>
      </Box>
    </StudentContextProvider>
  );
}

export default Students;
