import { call, takeEvery, put, all } from "redux-saga/effects";
import createCourseListAdapter from "../adapter/courses.adapter";
import createStudentListAdapter from "../adapter/student.adapter";
import { getCountriesSaga } from "../services/country.service";
import { getCoursesSaga } from "../services/course.service";
import { getStudentsSaga } from "../services/student.service";
import {
  fetchCoursesFailure,
  fetchCoursesInit,
  fetchCoursesSuccess,
} from "../redux/reducer/courses";
import {
  fetchCountriesFailure,
  fetchCountriesInit,
  fetchCountriesSuccess,
} from "../redux/reducer/countries";
import {
  fetchStudentsFailure,
  fetchStudentsInit,
  fetchStudentsSuccess,
} from "../redux/reducer/students";

function* fetchCourses() {
  try {
    const result = yield call(getCoursesSaga);
    const courseList = createCourseListAdapter(result.data);
    yield put(fetchCoursesSuccess(courseList));
  } catch (error) {
    yield put(fetchCoursesFailure());
  }
}

function* fetchCountries() {
  try {
    const result = yield call(getCountriesSaga);
    yield put(fetchCountriesSuccess(result.data));
  } catch (error) {
    yield put(fetchCountriesFailure());
  }
}

function* fetchStudents() {
  try {
    const result1 = yield call(getStudentsSaga);
    const result2 = yield call(getCountriesSaga);
    const studentList = createStudentListAdapter(result1.data, result2.data);
    yield put(fetchStudentsSuccess(studentList));
  } catch (error) {
    yield put(fetchStudentsFailure());
  }
}

function* rootSaga() {
  yield all([
    yield takeEvery(fetchCountriesInit.type, fetchCountries),
    yield takeEvery(fetchCoursesInit.type, fetchCourses),
    yield takeEvery(fetchStudentsInit.type, fetchStudents),
  ]);
}

export default rootSaga;
