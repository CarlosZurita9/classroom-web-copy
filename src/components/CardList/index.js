import React from "react";
import PropTypes from "prop-types";
import { Box, Grid, Stack } from "@mui/material";

function CardList({ layout, cards }) {
  if (layout === "grid") {
    // If using grid layout the cards property must be a card wrapped on a <Grid item />
    // Example:
    // <Grid item xs={12} sm={6} md={4}>
    //   <SubjectCard key={id} subject={rest} />
    // </Grid>
    return (
      <Box style={{ padding: "1em 1em" }}>
        <Grid
          container
          columnSpacing={{ xs: 1, sm: 2, md: 4 }}
          rowSpacing={{ xs: 1, sm: 2, md: 4 }}
        >
          {cards}
        </Grid>
      </Box>
    );
  }

  return (
    <Box style={{ padding: "1em 1em" }}>
      <Stack spacing={{ xs: 1, sm: 2, md: 4 }}>{cards}</Stack>
    </Box>
  );
}

CardList.defaultProps = {
  layout: "stack",
  cards: [],
};

CardList.propTypes = {
  layout: PropTypes.string,
  cards: PropTypes.arrayOf(PropTypes.node),
};

export default CardList;
