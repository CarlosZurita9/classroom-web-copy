import StudentContextProvider from "./StudentContextProvider";
import { useStudentContext } from "./studentContext";

export { StudentContextProvider, useStudentContext };
