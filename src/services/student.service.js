import axios from "axios";
import options from "../constants/student.service.constants";
import loadAbortAxios from "../helper/loadAbortAxios";

const getStudentByCourseId = (courseId) => {
  const controller = loadAbortAxios();
  return {
    call: async () =>
      axios.request(
        options.GET_STUDENT_BY_COURSE_ID(courseId),
        { signal: controller.signal },
        controller,
      ),
  };
};

const getUnregisteredStudents = () => {
  const controller = loadAbortAxios();
  return {
    call: async () =>
      axios.request(
        options.GET_UNREGISTERED_STUDENTS(),
        { signal: controller.signal },
        controller,
      ),
  };
};
const getStudents = () => {
  const controller = loadAbortAxios();
  return {
    call: async () =>
      axios.request(
        options.GET_STUDENTS(),
        { signal: controller.signal },
        controller,
      ),
  };
};

const postStudent = (data) => {
  const controller = loadAbortAxios();
  return {
    call: async () =>
      axios.request(
        options.POST_STUDENT(data),
        { signal: controller.signal },
        controller,
      ),
  };
};

const updateStudent = (register, id, courseId) => {
  const controller = loadAbortAxios();
  return {
    call: async () =>
      axios.request(
        options.UPDATE_STUDENT(register, id, courseId),
        { signal: controller.signal },
        controller,
      ),
  };
};

const getStudentsSaga = () => {
  const controller = loadAbortAxios();
  return axios.request(
    options.GET_STUDENTS(),
    { signal: controller.signal },
    controller,
  );
};

export {
  getStudentByCourseId,
  getUnregisteredStudents,
  postStudent,
  updateStudent,
  getStudents,
  getStudentsSaga,
};
