import React, { useMemo } from "react";
import {
  FormGroup,
  Grid,
  TextField,
  Typography,
  Box,
  MenuItem,
  Select,
  InputLabel,
  FormControl,
} from "@mui/material";

import EmailIcon from "@mui/icons-material/Email";
import CakeIcon from "@mui/icons-material/Cake";
import PublicIcon from "@mui/icons-material/Public";

import PropTypes from "prop-types";
import useFilterList from "../../hooks/useFilterList";
import StudentCard from "../StudentCard";
import CardList from "../CardList";

function FilterStudentList({ inputs, originalList }) {
  const { sections } = inputs;
  const initialValues = {};
  for (let i = 0; i < sections.length; i += 1) {
    sections[i].inputs.map(({ label, value }) => {
      initialValues[label] = value;
      return null;
    });
  }

  const { handleChange, values, filteredList } = useFilterList(
    initialValues,
    originalList,
  );

  const studentCards = useMemo(() => {
    const cards =
      filteredList !== undefined &&
      filteredList.length !== 0 &&
      filteredList.map((item) => {
        const bodyInfo = [
          {
            icon: <EmailIcon />,
            value: item.email,
          },
          {
            icon: <CakeIcon />,
            value: item.birthday,
          },
          {
            icon: <PublicIcon />,
            value: item.country,
          },
        ];
        const { email, birthday, ...rest } = item;
        return (
          <Grid key={item.id} item xs={12} sm={6} md={4}>
            <StudentCard student={rest} bodyInfo={bodyInfo} />
          </Grid>
        );
      });

    return cards;
  }, [filteredList]);

  const renderSearchInputs = () => {
    return (
      <>
        {sections.map((s) => {
          return (
            <div key={s.id}>
              <Grid container spacing={1}>
                {s.inputs.map((i) => {
                  return (
                    <Grid item key={i.id} xs={i.xs} sm={i.sm} md={i.md}>
                      {i.type === "text" ? (
                        <TextField
                          type={i.type}
                          placeholder={i.placeholder}
                          onChange={handleChange}
                          value={values[i.label]}
                          name={i.label}
                          fullWidth
                        />
                      ) : (
                        <FormControl fullWidth>
                          <InputLabel id={i.id}>{i.label}</InputLabel>
                          <Select
                            id={i.label}
                            value={values[i.label]}
                            label={i.label}
                            onChange={handleChange}
                            name={i.label}
                          >
                            {i.selectItems.map((item) => {
                              return (
                                <MenuItem key={item.value} value={item.value}>
                                  {item.label}
                                </MenuItem>
                              );
                            })}
                          </Select>
                        </FormControl>
                      )}
                    </Grid>
                  );
                })}
              </Grid>
            </div>
          );
        })}
      </>
    );
  };

  return (
    <>
      <FormGroup>{renderSearchInputs()}</FormGroup>
      {studentCards && <CardList layout="grid" cards={studentCards} />}
      {!studentCards && (
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
          minHeight="100px"
        >
          <Typography variant="h5">No students found!</Typography>
        </Box>
      )}
    </>
  );
}

FilterStudentList.propTypes = {
  inputs: PropTypes.shape({
    sections: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        inputs: PropTypes.arrayOf(
          PropTypes.shape({
            id: PropTypes.number,
            placeholder: PropTypes.string,
            label: PropTypes.string,
            type: PropTypes.string,
            xs: PropTypes.number,
            sm: PropTypes.number,
            md: PropTypes.number,
          }),
        ),
      }),
    ),
  }).isRequired,
  originalList: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      coureName: PropTypes.number,
    }),
  ).isRequired,
};

export default FilterStudentList;
