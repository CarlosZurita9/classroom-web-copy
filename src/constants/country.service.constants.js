import { BASE_API_URL, HEADERS } from "./api.constants";

const options = {
  GET_COUNTRIES: () => {
    return {
      method: "GET",
      url: `${BASE_API_URL}/Country`,
      headers: HEADERS,
    };
  },
};

export default options;
