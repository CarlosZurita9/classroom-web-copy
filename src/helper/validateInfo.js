export default function validateInfo(values, validations) {
  const errors = {};

  for (let i = 0; i < validations.length; i += 1) {
    const { label, validation } = validations[i];
    if (validation.isRequired && !values[label]) {
      errors[label] = `${label} is required`;
    } else if (validation.test(values)) {
      errors[label] = validation.message;
    }
  }

  return errors;
}
