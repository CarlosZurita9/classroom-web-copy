import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  FormGroup,
  Grid,
  TextField,
  Typography,
  MenuItem,
  Select,
  InputLabel,
  FormControl,
  Container,
  Stack,
} from "@mui/material";

import PropTypes from "prop-types";
import useFilterList from "../../hooks/useFilterList";
import CardList from "../CardList";
import { useCourseContext } from "../../contexts/courseContext";
import useFetchAndLoad from "../../hooks/useFetchAndLoad";
import {
  getUnregisteredStudents,
  updateStudent,
} from "../../services/student.service";
import createStudentListAdapter from "../../adapter/student.adapter";
import { fetchStudentsInit } from "../../redux/reducer/students";
import StudentsToDndCards from "../../helper/studentsToDndCard";

function FilterDnDList({ inputs }) {
  const cardHeight = 320;
  const [course, setCourse] = useCourseContext();
  const countries = useSelector((state) => state.countries.result);
  const [unregisteredStudents, setUnregisteredStudents] = useState([]);
  const [dragItem, setDragItem] = useState({});
  const { callEndpoint } = useFetchAndLoad();
  const [registeredStudents, setRegisteredStudents] = useState(course.students);
  const dispatch = useDispatch();

  useEffect(() => {
    const fetchUnregistered = async () => {
      let unregFetched;
      try {
        unregFetched = await callEndpoint(getUnregisteredStudents());
        const studentsData = createStudentListAdapter(
          unregFetched.data,
          countries,
        );
        setUnregisteredStudents(studentsData);
        return null;
      } catch (error) {
        return null;
      }
    };
    fetchUnregistered();
    // eslint-disable-next-line
  }, []);

  const handleUpdate = async (register, student) => {
    try {
      const response = await callEndpoint(
        updateStudent(register, student.id, course.id),
      );
      if (response.status === 201) {
        if (register) {
          const newStudent = {
            ...student,
            courseId: course.id,
            status: "Registered",
          };
          const studentList = [...registeredStudents, newStudent];
          setCourse({ ...course, students: studentList });
          setRegisteredStudents(studentList);
          setUnregisteredStudents(
            unregisteredStudents.filter((e) => e.id !== student.id),
          );
        } else {
          const newStudent = {
            ...student,
            courseId: null,
            status: "Unregistered",
          };
          const studentList = registeredStudents.filter(
            (e) => e.id !== student.id,
          );
          setCourse({ ...course, students: studentList });
          setUnregisteredStudents([...unregisteredStudents, newStudent]);
          setRegisteredStudents(studentList);
        }
        dispatch(fetchStudentsInit());
      }
      return null;
    } catch (error) {
      return null;
    }
  };

  const handleDragStart = (student) => {
    setDragItem(student);
  };

  const handleDragOver = (e) => {
    e.preventDefault();
  };

  const handleDrop = (register) => {
    if (
      (register && dragItem.courseId === null) ||
      (!register && dragItem.courseId !== null)
    ) {
      handleUpdate(register, dragItem);
    }
  };
  const { sections } = inputs;
  const initialValues = {};
  for (let i = 0; i < sections.length; i += 1) {
    sections[i].inputs.map(({ label, value }) => {
      initialValues[label] = value;
      return null;
    });
  }

  const students = [...unregisteredStudents, ...registeredStudents];

  const { handleChange, values, filteredList } = useFilterList(
    initialValues,
    students,
  );

  const registeredCards = StudentsToDndCards(
    registeredStudents.filter((s) => filteredList.includes(s)),
    handleDragStart,
  );
  const unregisteredCards = StudentsToDndCards(
    unregisteredStudents.filter((s) => filteredList.includes(s)),
    handleDragStart,
  );

  const maxCardCount =
    unregisteredCards.length > registeredCards.length
      ? unregisteredCards.length
      : registeredCards.length;

  const renderSearchInputs = () => {
    return (
      <>
        {sections.map((s) => {
          return (
            <div key={s.id}>
              <Grid container spacing={1}>
                {s.inputs.map((i) => {
                  return (
                    <Grid item key={i.id} xs={i.xs} sm={i.sm} md={i.md}>
                      {i.type === "text" ? (
                        <TextField
                          type={i.type}
                          placeholder={i.placeholder}
                          onChange={handleChange}
                          value={values[i.label]}
                          name={i.label}
                          fullWidth
                        />
                      ) : (
                        <FormControl fullWidth>
                          <InputLabel id={i.id}>{i.label}</InputLabel>
                          <Select
                            id={i.label}
                            value={values[i.label]}
                            label={i.label}
                            onChange={handleChange}
                            name={i.label}
                          >
                            {i.selectItems.map((item) => {
                              return (
                                <MenuItem key={item.value} value={item.value}>
                                  {item.label}
                                </MenuItem>
                              );
                            })}
                          </Select>
                        </FormControl>
                      )}
                    </Grid>
                  );
                })}
              </Grid>
            </div>
          );
        })}
      </>
    );
  };

  return (
    <>
      <FormGroup sx={{ mx: 10 }}>{renderSearchInputs()}</FormGroup>
      <Container fixed>
        <Grid container>
          <Grid item xs={6}>
            <div onDragOver={handleDragOver} onDrop={() => handleDrop(false)}>
              <Stack alignItems="center" minHeight={cardHeight * maxCardCount}>
                <Typography variant="h6">Students not registered</Typography>
                <CardList cards={unregisteredCards} />
              </Stack>
            </div>
          </Grid>
          <Grid item xs={6}>
            <div onDragOver={handleDragOver} onDrop={() => handleDrop(true)}>
              <Stack alignItems="center" minHeight={cardHeight * maxCardCount}>
                <Typography variant="h6">Students in the course</Typography>
                <CardList cards={registeredCards} />
              </Stack>
            </div>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}

FilterDnDList.propTypes = {
  inputs: PropTypes.shape({
    sections: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        inputs: PropTypes.arrayOf(
          PropTypes.shape({
            id: PropTypes.number,
            placeholder: PropTypes.string,
            label: PropTypes.string,
            type: PropTypes.string,
            xs: PropTypes.number,
            sm: PropTypes.number,
            md: PropTypes.number,
          }),
        ),
      }),
    ),
  }).isRequired,
};

export default FilterDnDList;
