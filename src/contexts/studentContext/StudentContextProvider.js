import React from "react";
import PropTypes from "prop-types";
import { StudentContext } from "./studentContext";

function StudentContextProvider({ value, children }) {
  return (
    <StudentContext.Provider value={value}>{children}</StudentContext.Provider>
  );
}

StudentContextProvider.propTypes = {
  value: PropTypes.arrayOf(Object).isRequired,
  children: PropTypes.node.isRequired,
};

export default StudentContextProvider;
