/* eslint-disable no-param-reassign */
import { createSlice } from "@reduxjs/toolkit";

const countriesSlice = createSlice({
  name: "countries",
  initialState: {
    result: [],
    isLoading: false,
  },
  reducers: {
    fetchCountriesInit: (state) => {
      state.isLoading = true;
    },
    fetchCountriesSuccess: (state, action) => {
      state.result = action.payload;
      state.isLoading = false;
    },
    fetchCountriesFailure: (state) => {
      state.isLoading = false;
    },
  },
});

export const {
  fetchCountriesInit,
  fetchCountriesFailure,
  fetchCountriesSuccess,
} = countriesSlice.actions;

export default countriesSlice.reducer;
