import { createContext, useContext } from "react";

const StudentPageContext = createContext({});

function useStudentPageContext() {
  return useContext(StudentPageContext);
}

export { StudentPageContext, useStudentPageContext };
