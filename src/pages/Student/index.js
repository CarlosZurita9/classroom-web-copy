import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { Box, Typography } from "@mui/material";
import StudentBar from "../../components/StudentBar";
import WeeklySchedule from "../../components/WeeklySchedule";
import { StudentPageContextProvider } from "../../contexts/studentPageContext";
import useFetchAndLoad from "../../hooks/useFetchAndLoad";
import { getSubjectsByCourseId } from "../../services/subject.service";
import createSubjectListAdapter from "../../adapter/subjects.adapter";

function Student() {
  const { studentId } = useParams();
  const students = useSelector((state) => state.students.result);
  const courses = useSelector((state) => state.courses.result);
  const { callEndpoint } = useFetchAndLoad();
  const [studentInfo, setStudentInfo] = useState({});

  useEffect(() => {
    let info = {};

    const fetchStudent = () => {
      const student = students.filter((s) => s.id === Number(studentId));
      info = { ...info, ...student[0] };
    };

    const fetchCourse = (courseId) => {
      const course = courses.filter((s) => s.id === courseId);
      info = { ...info, course: course[0] };
    };

    const fetchSubjects = async (courseId) => {
      let subjects;
      try {
        subjects = await callEndpoint(getSubjectsByCourseId(courseId));
        const subjectData = createSubjectListAdapter(subjects.data);
        info = { ...info, course: { ...info.course, subjects: subjectData } };
      } catch (error) {
        info = { ...info };
      }
    };

    const fetchAll = async () => {
      fetchStudent();
      if (info.courseId !== null) {
        fetchCourse(info.courseId);
        await fetchSubjects(info.courseId);
      }
      setStudentInfo(info);
    };

    fetchAll();
    // eslint-disable-next-line
  }, [students, courses]);

  if (studentInfo.name === undefined)
    return (
      <Box
        display="flex"
        justifyContent="center"
        alignItems="center"
        minHeight="100px"
      >
        <Typography variant="h5">Student Not Found!</Typography>
      </Box>
    );

  return (
    <StudentPageContextProvider value={studentInfo}>
      <StudentBar />
      <WeeklySchedule />
    </StudentPageContextProvider>
  );
}

export default Student;
