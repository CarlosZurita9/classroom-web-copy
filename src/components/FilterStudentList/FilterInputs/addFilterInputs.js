const addFilterInputs = (countries) => {
  return {
    sections: [
      {
        id: 1,
        title: "Filter by name",
        inputs: [
          {
            id: 1,
            placeholder: "Search...",
            label: "Filter Name",
            value: "",
            type: "text",
            xs: 6,
            sm: 6,
            md: 6,
          },
          {
            id: 2,
            placeholder: "",
            label: "Country",
            value: 0,
            type: "select",
            selectItems: [
              { value: 0, label: "All" },
              ...countries.map((c) => {
                return {
                  value: c.id,
                  label: c.name,
                };
              }),
            ],
            xs: 6,
            sm: 6,
            md: 6,
          },
        ],
      },
    ],
  };
};
export default addFilterInputs;
