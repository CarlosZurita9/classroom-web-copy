const courseFormInputs = (course) => {
  let title = "Add Course Form";
  let nameValue = "";
  let descValue = "";
  let startValue = "";
  let endValue = "";
  if (Object.keys(course).length !== 0) {
    const { courseName, description, startingDate, endingDate } = course;
    title = "Update Course Form";
    nameValue = courseName;
    descValue = description;
    startValue = new Date(startingDate).toISOString().slice(0, 10);
    endValue = new Date(endingDate).toISOString().slice(0, 10);
  }

  return {
    title,
    sections: [
      {
        id: 1,
        title: "Course data:",
        inputs: [
          {
            id: 1,
            placeholder: "Enter course name",
            label: "Course Name",
            value: nameValue,
            required: true,
            type: "text",
            xs: 12,
            sm: 6,
            md: 3,
            validation: {
              isRequired: true,
              test: () => {
                return false;
              },
              message: "",
            },
          },
          {
            id: 2,
            placeholder: null,
            label: "Description",
            value: descValue,
            required: false,
            type: "text",
            xs: 12,
            sm: 6,
            md: 3,
            validation: {
              isRequired: false,
              test: () => {
                return false;
              },
              message: "",
            },
          },
          {
            id: 3,
            placeholder: null,
            label: "Course Image",
            value: "",
            required: false,
            type: "file",
            xs: 12,
            sm: 6,
            md: 3,
            validation: {
              isRequired: false,
              test: () => {
                return false;
              },
              message: "",
            },
          },
        ],
      },
      {
        id: 2,
        title: "Dates:",
        inputs: [
          {
            id: 1,
            placeholder: null,
            label: "Start Date",
            required: true,
            value: startValue,
            type: "date",
            xs: 12,
            sm: 12,
            md: 2,
            validation: {
              isRequired: true,
              test: () => {
                return false;
              },
              message: "",
            },
          },
          {
            id: 2,
            placeholder: null,
            label: "End Date",
            value: endValue,
            required: true,
            type: "date",
            xs: 12,
            sm: 12,
            md: 2,
            validation: {
              isRequired: true,
              test: () => {
                return false;
              },
              message: "",
            },
          },
        ],
      },
    ],
    radios: [],
  };
};

export default courseFormInputs;
