import React, { useEffect } from "react";
import { Route, Routes } from "react-router-dom";
import { useDispatch } from "react-redux";
import { Box } from "@mui/material";
import Navbar from "./components/Navbar";
import { Courses, NotFound, Students, Course, Student } from "./pages";
import "./App.css";
import { fetchCoursesInit } from "./redux/reducer/courses";
import { fetchCountriesInit } from "./redux/reducer/countries";
import { fetchStudentsInit } from "./redux/reducer/students";

const sections = [
  { page: "Course", link: "/" },
  { page: "Student", link: "/students" },
];

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchCoursesInit());
    dispatch(fetchCountriesInit());
    dispatch(fetchStudentsInit());
    // eslint-disable-next-line
  }, []);

  return (
    <Box m="auto">
      <Navbar title="ClassRoom" sections={sections} />
      <Routes>
        <Route path="/course/:courseId" element={<Course />} />
        <Route path="/students" element={<Students />} />
        <Route path="/students/:studentId" element={<Student />} />
        <Route path="/" element={<Courses />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </Box>
  );
}

export default App;
