const API_SCHEME = process.env.REACT_APP_SERVER_SCHEME;
const BASE_API_URL = `${API_SCHEME}${process.env.REACT_APP_SERVER_API}`;

const HEADERS = {
  "Access-Control-Allow-Origin": "*",
};

export { BASE_API_URL, HEADERS, API_SCHEME };
