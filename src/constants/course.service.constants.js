import { BASE_API_URL, HEADERS } from "./api.constants";

const options = {
  GET_COURSES: () => {
    return {
      method: "GET",
      url: `${BASE_API_URL}/Course`,
      headers: HEADERS,
    };
  },
  GET_COURSE_BY_ID: (id) => {
    return {
      method: "GET",
      url: `${BASE_API_URL}/Course/${id}`,
      headers: HEADERS,
    };
  },
  POST_COURSE: (course) => {
    return {
      method: "POST",
      url: `${BASE_API_URL}/Course/`,
      data: course,
      headers: HEADERS,
    };
  },
  PUT_COURSE: (id, course) => {
    return {
      method: "PUT",
      url: `${BASE_API_URL}/Course/${id}`,
      data: course,
      headers: HEADERS,
    };
  },
  DELETE_COURSE: (id) => {
    return {
      method: "DELETE",
      url: `${BASE_API_URL}/Course/${id}`,
      headers: HEADERS,
    };
  },
};

export default options;
