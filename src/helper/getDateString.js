const getDateString = (dateString) =>
  new Date(dateString.slice(0, 10).replace(/-/g, "/")).toLocaleDateString(
    "en-US",
    {
      year: "numeric",
      month: "long",
      day: "numeric",
    },
  );

export default getDateString;
