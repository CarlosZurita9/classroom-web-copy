import axios from "axios";
import options from "../constants/country.service.constants";
import loadAbortAxios from "../helper/loadAbortAxios";

const getCountries = () => {
  const controller = loadAbortAxios();
  return {
    call: async () =>
      axios.request(
        options.GET_COUNTRIES(),
        { signal: controller.signal },
        controller,
      ),
  };
};

const getCountriesSaga = () => {
  const controller = loadAbortAxios();
  return axios.request(
    options.GET_COUNTRIES(),
    { signal: controller.signal },
    controller,
  );
};

export { getCountries, getCountriesSaga };
