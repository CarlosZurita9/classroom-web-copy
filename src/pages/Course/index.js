import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Grid, Box, Button, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";

import EmailIcon from "@mui/icons-material/Email";
import CakeIcon from "@mui/icons-material/Cake";
import PublicIcon from "@mui/icons-material/Public";

import CardList from "../../components/CardList";
import createSubjectListAdapter from "../../adapter/subjects.adapter";
import SubjectCard from "../../components/SubjectCard";
import useFetchAndLoad from "../../hooks/useFetchAndLoad";
import { getSubjectsByCourseId } from "../../services/subject.service";
import { getStudentByCourseId } from "../../services/student.service";
import CourseBar from "../../components/CourseBar";
import { CourseContextProvider } from "../../contexts/courseContext";
import SeparatorBar from "../../components/SeparatorBar";
import StudentCard from "../../components/StudentCard";
import createStudentListAdapter from "../../adapter/student.adapter";
import { getCountries } from "../../services/country.service";
import {
  displaySubjectsFalse,
  displaySubjectsTrue,
} from "../../redux/reducer/students";

function Course() {
  const { courseId } = useParams();
  const courses = useSelector((state) => state.courses.result);
  const showSubjects = useSelector((state) => state.students.displaySubjects);
  const dispatch = useDispatch();
  const { callEndpoint } = useFetchAndLoad();
  const [course, setCourse] = useState({});
  const [isAddingSubject, setIsAddingSubject] = useState(false);
  const [isAddingStudent, setIsAddingStudent] = useState(false);

  useEffect(() => {
    let currentCourse = {};

    const fetchCountry = async () => {
      let countriesFetched;
      try {
        countriesFetched = await callEndpoint(getCountries());
        return countriesFetched.data;
      } catch (error) {
        return null;
      }
    };

    const fetchStudents = async () => {
      let studentsList;
      try {
        const countries = await fetchCountry();
        studentsList = await callEndpoint(getStudentByCourseId(courseId));
        const studentsData = createStudentListAdapter(
          studentsList.data,
          countries,
        );
        currentCourse = { ...currentCourse, students: studentsData };
      } catch (error) {
        currentCourse = { ...currentCourse, students: [] };
      }
    };

    const fetchSubjects = async () => {
      let subjectList;
      try {
        subjectList = await callEndpoint(getSubjectsByCourseId(courseId));
        const subjectsData = createSubjectListAdapter(subjectList.data);
        currentCourse = { ...currentCourse, subjects: subjectsData };
      } catch (error) {
        currentCourse = { ...currentCourse, subjects: [] };
      }
    };

    const fetchCourse = () => {
      const coursefetched = courses.filter((c) => c.id === Number(courseId));
      currentCourse = { ...currentCourse, ...coursefetched[0] };
    };

    const fetchAll = async () => {
      fetchCourse();
      await fetchSubjects();
      await fetchStudents();
      setCourse(currentCourse);
    };

    fetchAll();
    // eslint-disable-next-line
  }, [courses]);

  const subjectCards =
    course.subjects !== undefined &&
    course.subjects.length !== 0 &&
    course.subjects.map((item) => {
      const { id, ...rest } = item;
      return (
        <Grid key={id} item xs={12} sm={6} md={4}>
          <SubjectCard subject={rest} />
        </Grid>
      );
    });

  const renderSubjectsCards = () => {
    if (subjectCards) {
      return <CardList layout="grid" cards={subjectCards} />;
    }
    return (
      <Box
        display="flex"
        justifyContent="center"
        alignItems="center"
        minHeight="100px"
      >
        <Typography variant="h5">No subjects registered yet!</Typography>
      </Box>
    );
  };

  const studentCards =
    course.students !== undefined &&
    course.students.length !== 0 &&
    course.students.map((item) => {
      const bodyInfo = [
        {
          icon: <EmailIcon />,
          value: item.email,
        },
        {
          icon: <CakeIcon />,
          value: item.birthday,
        },
        {
          icon: <PublicIcon />,
          value: item.country,
        },
      ];
      const { email, birthday, ...rest } = item;
      return (
        <Grid key={item.id} item xs={12} sm={6} md={4}>
          <StudentCard student={rest} bodyInfo={bodyInfo} />
        </Grid>
      );
    });

  const renderStudentCards = () => {
    if (studentCards) {
      return <CardList layout="grid" cards={studentCards} />;
    }
    return (
      <Box
        display="flex"
        justifyContent="center"
        alignItems="center"
        minHeight="100px"
      >
        <Typography variant="h5">No students registered yet!</Typography>
      </Box>
    );
  };

  if (course.courseName === undefined)
    return (
      <Box
        display="flex"
        justifyContent="center"
        alignItems="center"
        minHeight="100px"
      >
        <Typography variant="h5">Course Not Found!</Typography>
      </Box>
    );

  const style = {
    selected: {
      fontSize: "16px",
      fontWeight: "bolder",
    },
    unselected: {
      fontSize: "16px",
      fontWeight: "bolder",
      backgroundColor: "grey",
    },
  };

  return (
    <CourseContextProvider value={[course, setCourse]}>
      <CourseBar
        isAddingSubject={isAddingSubject}
        onAddSubject={setIsAddingSubject}
        isAddingStudent={isAddingStudent}
        onAddStudent={setIsAddingStudent}
      />
      {!isAddingStudent && (
        <SeparatorBar>
          <Button
            variant="contained"
            style={!showSubjects ? style.selected : style.unselected}
            onClick={() => dispatch(displaySubjectsTrue())}
          >
            Subject List
          </Button>
          <Button
            variant="contained"
            style={showSubjects ? style.selected : style.unselected}
            onClick={() => dispatch(displaySubjectsFalse())}
          >
            Student List
          </Button>
        </SeparatorBar>
      )}
      <Box
        sx={{
          marginTop: "8px",
        }}
      >
        {!isAddingStudent && showSubjects && renderSubjectsCards()}
        {!isAddingStudent && !showSubjects && renderStudentCards()}
      </Box>
    </CourseContextProvider>
  );
}

export default Course;
