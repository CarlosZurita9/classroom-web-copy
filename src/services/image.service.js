import axios from "axios";
import options from "../constants/image.service.constants";
import loadAbortAxios from "../helper/loadAbortAxios";

const createImage = (folder, fileName, image) => {
  const controller = loadAbortAxios();
  return {
    call: async () =>
      axios.request(
        options.POST_IMAGE(folder, fileName, image),
        { signal: controller.signal },
        controller,
      ),
  };
};

export default createImage;
