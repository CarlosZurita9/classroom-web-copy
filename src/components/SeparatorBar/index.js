import React from "react";
import PropTypes from "prop-types";
import { AppBar, Box, Container, Toolbar, Typography } from "@mui/material";

function SeparatorBar({ title, children }) {
  const style = {
    appbar: {
      background: "#5885AF",
    },
  };

  return (
    <AppBar position="static" style={style.appbar}>
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Typography
            variant="h6"
            noWrap
            component="a"
            href="/"
            sx={{
              mr: 2,
              display: { xs: "flex", md: "flex" },
              fontFamily: "monospace",
              fontWeight: 700,
              letterSpacing: ".3rem",
              color: "inherit",
              textDecoration: "none",
            }}
          >
            {title}
          </Typography>
          <Box
            sx={{
              flexGrow: 1,
              display: { xs: "flex", md: "flex" },
              justifyContent: "flex-end",
              gap: "1rem",
            }}
          >
            {children}
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
}

SeparatorBar.defaultProps = {
  title: "",
  children: <div />,
};

SeparatorBar.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node,
};

export default SeparatorBar;
