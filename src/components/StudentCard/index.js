import React from "react";
import {
  Avatar,
  Badge,
  Box,
  Card,
  CardMedia,
  Stack,
  Tooltip,
  Typography,
} from "@mui/material";
import { Link } from "react-router-dom";
import AccountBoxIcon from "@mui/icons-material/AccountBox";
import CircleOutlinedIcon from "@mui/icons-material/CircleOutlined";
import PropTypes from "prop-types";
import { BASE_API_URL } from "../../constants/api.constants";

function StudentCard({ student, bodyInfo }) {
  const style = {
    primaryText: {
      color: "#000000",
      fontWeight: 700,
      fontSize: 20,
    },
    secondaryText: {
      fontSize: "16",
      color: "#6B7071",
    },
    primaryColor: {
      color: "#3BBCC1",
    },
  };

  return (
    <Card
      sx={{
        height: 310,
        pr: 3,
        borderLeft: 4,
        borderColor: "#3BBCC1",
        boxShadow: 6,
        borderRadius: 2,
      }}
    >
      <Stack direction="row">
        <Badge
          overlap="circular"
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          badgeContent={
            <Avatar
              sx={{
                height: 50,
                width: 50,
              }}
              alt={student.name}
              src={`${BASE_API_URL}/Images/country/${student.country}.png`}
            />
          }
        >
          <Box
            sx={{
              p: 3,
            }}
          >
            <CardMedia
              sx={{
                width: { xs: 180, sm: 150, md: 135, lg: 160 },
                height: { xs: 180, sm: 150, md: 135, lg: 160 },
                borderRadius: 2,
              }}
              component="img"
              image={student.image}
              alt={student.name}
            />
          </Box>
        </Badge>
        <Stack
          alignItems="flex-start"
          spacing={2}
          sx={{
            // background: "#00ff00",
            pt: 3,
            pl: 1,
            width: { xs: "70%", sm: "37%", md: "56%" },
          }}
        >
          <Typography style={style.primaryText}> Info </Typography>
          {bodyInfo.map(({ value, icon }) => (
            <Stack
              key={value}
              direction="row"
              spacing={1}
              style={style.primaryColor}
              sx={{
                width: "95%",
              }}
            >
              {icon}
              <Tooltip title={value} followCursor>
                <Typography
                  noWrap
                  style={style.secondaryText}
                  sx={{
                    // backgroundColor: "#ff0000",
                    textOverflow: "ellipsis",
                    width: "100%",
                  }}
                >
                  {value}
                </Typography>
              </Tooltip>
            </Stack>
          ))}
        </Stack>
      </Stack>
      <Stack
        direction="row"
        pl={3}
        sx={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <Stack>
          <Typography style={style.primaryText}>{student.name}</Typography>
          <Typography style={style.secondaryText}>Student</Typography>
        </Stack>
        <Stack spacing={1}>
          <Stack direction="row" spacing={1} mt={2} style={style.primaryColor}>
            <Typography
              style={style.secondaryText}
              sx={{
                fontSize: 15,
              }}
            >
              {student.status}
            </Typography>
            <CircleOutlinedIcon style={style.primaryColor} />
          </Stack>
          <Link to={`/students/${student.id}`} style={style.secondaryText}>
            <Stack
              direction="row"
              spacing={1}
              style={style.primaryColor}
              sx={{ display: "flex", justifyContent: "flex-end" }}
            >
              <Typography
                style={style.secondaryText}
                sx={{
                  fontSize: 15,
                  display: "flex",
                  justifyContent: "space-between",
                }}
              >
                Profile
              </Typography>
              <AccountBoxIcon style={style.primaryColor} />
            </Stack>
          </Link>
        </Stack>
      </Stack>
    </Card>
  );
}

StudentCard.propTypes = {
  student: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    country: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired,
  }).isRequired,
  bodyInfo: PropTypes.arrayOf(
    PropTypes.shape({
      icon: PropTypes.node.isRequired,
      value: PropTypes.string.isRequired,
    }),
  ).isRequired,
};

export default StudentCard;
