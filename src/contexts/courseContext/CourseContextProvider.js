import React from "react";
import PropTypes from "prop-types";
import { CourseContext } from "./courseContext";

function CourseContextProvider({ value, children }) {
  return (
    <CourseContext.Provider value={value}>{children}</CourseContext.Provider>
  );
}

// value must be an array of two elements composed by:
// 0: PropTypes.shape({
//   courseName: PropTypes.string,
//   description: PropTypes.string,
//   image: PropTypes.string,
//   startingDate: PropTypes.string,
//   endingDate: PropTypes.string,
//   subjects: PropTypes.arrayOf(Object),
// }),
// 1: PropTypes.fun,
CourseContextProvider.propTypes = {
  value: PropTypes.arrayOf(Object).isRequired,
  children: PropTypes.node.isRequired,
};

export default CourseContextProvider;
