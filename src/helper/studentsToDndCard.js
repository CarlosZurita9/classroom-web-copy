import React from "react";
import EmailIcon from "@mui/icons-material/Email";
import CakeIcon from "@mui/icons-material/Cake";
import PublicIcon from "@mui/icons-material/Public";
import StudentCard from "../components/StudentCard";

function StudentsToDndCards(students, handleDragStart) {
  if (students === undefined || students.length === 0) return [];
  return students.map((item) => {
    const bodyInfo = [
      {
        icon: <EmailIcon />,
        value: item.email,
      },
      {
        icon: <CakeIcon />,
        value: item.birthday,
      },
      {
        icon: <PublicIcon />,
        value: item.country,
      },
    ];
    const { email, birthday, ...rest } = item;
    return (
      <div key={item.id} draggable onDragStart={() => handleDragStart(item)}>
        <StudentCard student={rest} bodyInfo={bodyInfo} />
      </div>
    );
  });
}

export default StudentsToDndCards;
