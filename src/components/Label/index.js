import React from "react";
import PropTypes from "prop-types";
import { AppBar, Container, Toolbar, Typography, Button } from "@mui/material";

const style = {
  appbar: {
    background: "#2E3B55",
  },
  primary: {
    backgroundColor: "#33a7ff",
    fontSize: "16px",
    fontWeight: "bolder",
  },
  secondary: {
    backgroundColor: "#bbddf7",
    fontSize: "16px",
    fontWeight: "bolder",
  },
};

function Label({ title, acctionBtn, handleToggle }) {
  return (
    <AppBar position="static" style={style.appbar}>
      <Container maxWidth="xl">
        <Toolbar
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <Typography
            variant="h6"
            noWrap
            sx={{
              mr: 2,
              fontFamily: "monospace",
              letterSpacing: ".3rem",
              color: "inherit",
            }}
          >
            {title}
          </Typography>
          {acctionBtn.length !== 0 &&
            acctionBtn.map(({ name }) => (
              <Button variant="contained" key={name} onClick={handleToggle}>
                {name}
              </Button>
            ))}
        </Toolbar>
      </Container>
    </AppBar>
  );
}

// https://github.com/mui/material-ui/tree/v5.8.5/docs/data/material/getting-started/templates/blog
// https://github.dev/mui/material-ui/tree/v5.8.5/docs/data/material/getting-started/templates/blog
// https://mui.com/material-ui/react-grid/

Label.defaultProps = {
  acctionBtn: [],
  handleToggle: () => {},
};

Label.propTypes = {
  title: PropTypes.string.isRequired,
  handleToggle: PropTypes.func,
  acctionBtn: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
    }),
  ),
};

export default Label;
