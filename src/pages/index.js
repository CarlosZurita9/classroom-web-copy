import Courses from "./Courses";
import Home from "./Home";
import NotFound from "./NotFound";
import Students from "./Students";
import Student from "./Student";
import Course from "./Course";

export { Courses, Home, NotFound, Students, Course, Student };
