import React from "react";
import {
  Avatar,
  Badge,
  Box,
  Button,
  Card,
  CardMedia,
  Stack,
  Typography,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import EmailIcon from "@mui/icons-material/Email";
import CakeIcon from "@mui/icons-material/Cake";
import PublicIcon from "@mui/icons-material/Public";
import CircleOutlinedIcon from "@mui/icons-material/CircleOutlined";
import EventAvailableIcon from "@mui/icons-material/EventAvailable";
import EventBusyIcon from "@mui/icons-material/EventBusy";
import SchoolIcon from "@mui/icons-material/School";
import { BASE_API_URL } from "../../constants/api.constants";
import { useStudentPageContext } from "../../contexts/studentPageContext";
import SeparatorBar from "../SeparatorBar";

function StudentBar() {
  const { course, ...student } = useStudentPageContext();
  const navigate = useNavigate();
  const style = {
    primary: {
      fontSize: "16px",
      fontWeight: "bolder",
    },
    icon: {
      marginRight: 1,
      color: "#009698",
    },
  };

  return (
    <>
      <SeparatorBar title={student.name}>
        <Button
          variant="contained"
          style={style.primary}
          onClick={() => {
            navigate("/students");
          }}
        >
          Back to list
        </Button>
      </SeparatorBar>
      <Card>
        <Stack direction="row">
          <Badge
            overlap="circular"
            anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
            badgeContent={
              <Avatar
                sx={{
                  height: 50,
                  width: 50,
                }}
                alt={student.name}
                src={`${BASE_API_URL}/Images/country/${student.country}.png`}
              />
            }
          >
            <Box
              sx={{
                p: 3,
              }}
            >
              <CardMedia
                sx={{
                  width: { xs: 180, sm: 150, md: 135, lg: 160 },
                  height: { xs: 180, sm: 150, md: 135, lg: 160 },
                  borderRadius: 2,
                }}
                component="img"
                image={student.image}
                alt={student.name}
              />
            </Box>
          </Badge>
          <Box display="flex" justifyContent="flex-end" sx={{ p: 3 }}>
            <Stack spacing={1}>
              <Typography variant="h4" component="h2">
                Info
              </Typography>
              <Stack direction="row">
                <EmailIcon sx={style.icon} />
                <Typography>{student.email}</Typography>
              </Stack>
              <Stack direction="row">
                <CakeIcon sx={style.icon} />
                <Typography>{student.birthday}</Typography>
              </Stack>
              <Stack direction="row">
                <PublicIcon sx={style.icon} />
                <Typography>{student.country}</Typography>
              </Stack>
              <Stack direction="row">
                <CircleOutlinedIcon sx={style.icon} />
                <Typography>{student.status}</Typography>
              </Stack>
            </Stack>
          </Box>
          {course !== undefined && (
            <Box display="flex" justifyContent="flex-end" sx={{ p: 3 }}>
              <Stack spacing={1}>
                <Typography variant="h4" component="h2">
                  Course
                </Typography>
                <Stack direction="row">
                  <SchoolIcon sx={style.icon} />
                  <Typography>{course.courseName}</Typography>
                </Stack>
                <Stack direction="row">
                  <EventAvailableIcon sx={style.icon} />
                  <Typography>{course.startingDate}</Typography>
                </Stack>
                <Stack direction="row">
                  <EventBusyIcon sx={style.icon} />
                  <Typography>{course.endingDate}</Typography>
                </Stack>
              </Stack>
            </Box>
          )}
        </Stack>
      </Card>
      <SeparatorBar title="Schedule and Subjects" />
    </>
  );
}

export default StudentBar;
