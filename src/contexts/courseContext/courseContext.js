import { createContext, useContext } from "react";

const CourseContext = createContext({});

function useCourseContext() {
  return useContext(CourseContext);
}

export { CourseContext, useCourseContext };
