const addFilterInputs = {
  sections: [
    {
      id: 1,
      title: "Filter by name",
      inputs: [
        {
          id: 1,
          placeholder: "Search...",
          label: "Filter Name",
          value: "",
          type: "text",
          xs: 12,
          sm: 12,
          md: 12,
        },
      ],
    },
    // {
    //   id: 2,
    //   title: "Filter by date:",
    //   inputs: [
    //     {
    //       id: 1,
    //       placeholder: null,
    //       label: "From Date",
    //       value: "",
    //       type: "date",
    //       xs: 12,
    //       sm: 12,
    //       md: 2,
    //     },
    //     // {
    //     //   id: 2,
    //     //   placeholder: null,
    //     //   label: "To Date",
    //     //   value: "",
    //     //   type: "date",
    //     //   xs: 12,
    //     //   sm: 6,
    //     //   md: 2,
    //     // },
    //   ],
    // },
  ],
  radios: [
    {
      id: 1,
      title: "Filter by status",
      values: [
        { id: 1, value: 1, label: "Active" },
        { id: 2, value: 2, label: "Inactive" },
        { id: 3, value: 4, label: "Finished" },
      ],
    },
  ],
};

export default addFilterInputs;
