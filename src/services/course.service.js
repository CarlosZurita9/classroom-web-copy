import axios from "axios";
import options from "../constants/course.service.constants";
import loadAbortAxios from "../helper/loadAbortAxios";

const getCourses = () => {
  const controller = loadAbortAxios();
  return {
    call: async () =>
      axios.request(
        options.GET_COURSES(),
        { signal: controller.signal },
        controller,
      ),
  };
};

const getCourseById = (id) => {
  const controller = loadAbortAxios();
  return {
    call: async () =>
      axios.request(
        options.GET_COURSE_BY_ID(id),
        { signal: controller.signal },
        controller,
      ),
  };
};

const postCourse = (data) => {
  const controller = loadAbortAxios();
  return {
    call: async () =>
      axios.request(
        options.POST_COURSE(data),
        { signal: controller.signal },
        controller,
      ),
  };
};

const updateCourse = (id, data) => {
  const controller = loadAbortAxios();
  return {
    call: async () =>
      axios.request(
        options.PUT_COURSE(id, data),
        { signal: controller.signal },
        controller,
      ),
  };
};

const deleteCourse = (id) => {
  const controller = loadAbortAxios();
  return {
    call: async () =>
      axios.request(
        options.DELETE_COURSE(id),
        { signal: controller.signal },
        controller,
      ),
  };
};

const getCoursesSaga = () => {
  const controller = loadAbortAxios();
  return axios.request(
    options.GET_COURSES(),
    { signal: controller.signal },
    controller,
  );
};

export {
  getCourses,
  getCourseById,
  postCourse,
  updateCourse,
  deleteCourse,
  getCoursesSaga,
};
