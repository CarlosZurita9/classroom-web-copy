const studentFormInputs = (countries) => {
  return {
    title: "Add Student form",
    sections: [
      {
        id: 1,
        title: "Personal info:",
        inputs: [
          {
            id: 1,
            placeholder: "Enter first name",
            label: "First Name",
            value: "",
            required: true,
            type: "text",
            xs: 12,
            sm: 6,
            md: 3,
            validation: {
              isRequired: true,
              test: (values) => {
                return values["First Name"].length > 25;
              },
              message: "First name must have less than 25 characters",
            },
          },
          {
            id: 2,
            placeholder: "Enter first name",
            label: "Last Name",
            value: "",
            required: true,
            type: "text",
            xs: 12,
            sm: 6,
            md: 3,
            validation: {
              isRequired: true,
              test: (values) => {
                return values["Last Name"].length > 25;
              },
              message: "Last name must have less than 25 characters",
            },
          },
          {
            id: 3,
            placeholder: "Enter email",
            label: "Email",
            value: "",
            required: true,
            type: "text",
            xs: 12,
            sm: 6,
            md: 3,
            validation: {
              isRequired: true,
              test: (values) => {
                return !/\S+@\S+\.\S+/.test(values.Email);
              },
              message: "Email must be written in a correct format abc@xyz.com",
            },
          },
        ],
      },
      {
        id: 2,
        title: "Additional info:",
        inputs: [
          {
            id: 1,
            placeholder: null,
            label: "Birthday",
            value: "",
            required: true,
            type: "date",
            xs: 12,
            sm: 6,
            md: 3,
            validation: {
              isRequired: true,
              test: () => {
                return false;
              },
              message: "",
            },
          },
          {
            id: 2,
            placeholder: null,
            label: "Student Image",
            value: "",
            required: true,
            type: "file",
            xs: 12,
            sm: 6,
            md: 3,
            validation: {
              isRequired: true,
              test: () => {
                return false;
              },
              message: "",
            },
          },
          {
            id: 3,
            placeholder: "",
            label: "Country",
            value: 1,
            required: true,
            type: "select",
            selectItems: countries.map((c) => {
              return {
                value: c.id,
                label: c.name,
              };
            }),
            xs: 12,
            sm: 6,
            md: 3,
            validation: {
              isRequired: true,
              test: () => {
                return false;
              },
              message: "",
            },
          },
        ],
      },
    ],
    radios: [],
  };
};

export default studentFormInputs;
