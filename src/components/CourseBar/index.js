import React, { useState } from "react";
import PropTypes from "prop-types";
import {
  Button,
  Typography,
  Container,
  Stack,
  Card,
  CardMedia,
} from "@mui/material/";
import CircleIcon from "@mui/icons-material/Circle";

import SeparatorBar from "../SeparatorBar";
import { useCourseContext } from "../../contexts/courseContext";
import Form from "../Form";
import addSubjectFormInputs from "../Form/FormInputs/addSubjectFormInputs";
import useFetchAndLoad from "../../hooks/useFetchAndLoad";
import createImage from "../../services/image.service";
import { createSubject } from "../../services/subject.service";
import createSubjectListAdapter from "../../adapter/subjects.adapter";
import ErrorCard from "../ErrorCard";
import { API_SCHEME } from "../../constants/api.constants";
import AddStudentDnD from "../AddStudentDnD";

function CourseBar({
  isAddingSubject,
  onAddSubject,
  isAddingStudent,
  onAddStudent,
}) {
  const [course, setCourse] = useCourseContext();
  const [errorMessage, setErrorMessage] = useState("");
  const { callEndpoint } = useFetchAndLoad();
  const style = {
    primary: {
      fontSize: "16px",
      fontWeight: "bolder",
    },
    secundary: {
      fontSize: "16px",
      fontWeight: "bolder",
      background: "grey",
    },
  };

  const getColor = (state) => {
    if (state === "Active") return "#4fd92d";
    if (state === "Inactive") return "#f3a026";
    return "#c51515";
  };

  const handleSubmit = async (values) => {
    try {
      const image = values["Subject Image"][0];
      const subjectName = values["Subject Name"];
      const formData = new FormData();
      formData.append("files", image, image.name);
      const imagePost = await callEndpoint(
        createImage("subject", `${course.id}-${subjectName}`, formData),
      );
      const subject = {
        subjectName,
        startingDate: values["Start Date"],
        trainerName: values["Trainer Name"],
        startTime: values["Start Hour"],
        endTime: values["End Hour"],
        daysOfWeek: values.Days,
        image: `${API_SCHEME}${imagePost.data}`,
        courseId: course.id,
      };
      const newSubject = await callEndpoint(createSubject(subject));
      const subjectsData = createSubjectListAdapter([newSubject.data]);
      setCourse({ ...course, subjects: [...course.subjects, ...subjectsData] });
      onAddSubject(false);
      setErrorMessage("");
    } catch (error) {
      if (error.response.data.length > 0) {
        setErrorMessage(`${error.response.data}`);
      } else {
        setErrorMessage(`${error.response.statusText}`);
      }
    }
  };

  return (
    <>
      <SeparatorBar title={course.courseName}>
        <Button
          variant="contained"
          style={isAddingSubject ? style.secundary : style.primary}
          onClick={() => {
            onAddStudent(false);
            onAddSubject(!isAddingSubject);
          }}
          disabled={course.status === "Finished"}
        >
          Add Subjects
        </Button>
        <Button
          variant="contained"
          style={isAddingStudent ? style.secundary : style.primary}
          onClick={() => {
            onAddSubject(false);
            onAddStudent(!isAddingStudent);
          }}
          disabled={course.status === "Finished"}
        >
          Add Students
        </Button>
      </SeparatorBar>
      <Card sx={{ padding: "8px" }}>
        <Stack direction="row">
          <CardMedia
            component="img"
            sx={{ width: 151 }}
            image={course.image}
            alt="Course image"
          />
          <Container>
            <Typography>{course.description}</Typography>
            <Typography>{`Starts on ${course.startingDate}`}</Typography>
            <Typography>{`Ends on ${course.endingDate}`}</Typography>
            <Stack direction="row" spacing={1}>
              <Typography>state:</Typography>
              <Typography>{course.status}</Typography>
              <CircleIcon style={{ color: getColor(course.status) }} />
            </Stack>
          </Container>
        </Stack>
      </Card>
      {errorMessage && <ErrorCard message={errorMessage} />}
      {isAddingSubject && !isAddingStudent && (
        <Form inputs={addSubjectFormInputs} onSubmit={handleSubmit} />
      )}
      {isAddingStudent && !isAddingSubject && <AddStudentDnD />}
    </>
  );
}

CourseBar.propTypes = {
  isAddingSubject: PropTypes.bool.isRequired,
  onAddSubject: PropTypes.func.isRequired,
  isAddingStudent: PropTypes.bool.isRequired,
  onAddStudent: PropTypes.func.isRequired,
};

export default CourseBar;
