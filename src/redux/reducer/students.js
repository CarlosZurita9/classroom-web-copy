/* eslint-disable no-param-reassign */
import { createSlice } from "@reduxjs/toolkit";

const studentsSlice = createSlice({
  name: "students",
  initialState: {
    result: [],
    displaySubjects: true,
    isLoading: false,
  },
  reducers: {
    fetchStudentsInit: (state) => {
      state.isLoading = true;
    },
    fetchStudentsSuccess: (state, action) => {
      state.result = action.payload;
      state.isLoading = false;
    },
    fetchStudentsFailure: (state) => {
      state.isLoading = false;
    },
    displaySubjectsToggle: (state) => {
      state.displaySubjects = !state.displaySubjects;
    },
    displaySubjectsTrue: (state) => {
      state.displaySubjects = true;
    },
    displaySubjectsFalse: (state) => {
      state.displaySubjects = false;
    },
  },
});

export const {
  fetchStudentsInit,
  fetchStudentsFailure,
  fetchStudentsSuccess,
  displaySubjectsToggle,
  displaySubjectsTrue,
  displaySubjectsFalse,
} = studentsSlice.actions;

export default studentsSlice.reducer;
