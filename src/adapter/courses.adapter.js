import getDateString from "../helper/getDateString";
import getCourseState from "../helper/getCourseState";
import NoImage from "../assets/images/no-image.jpg";

const validateImage = (image) => {
  if (image === null) return NoImage;
  if (image.includes("http")) {
    return image;
  }
  return NoImage;
};

const createCourseListAdapter = (data) => {
  const courseList = [];

  if (data !== undefined) {
    for (let i = 0; i < data.length; i += 1) {
      courseList.push({
        id: data[i].id,
        courseName: data[i].courseName,
        description: data[i].description,
        startingDate: getDateString(data[i].startingDate),
        endingDate: getDateString(data[i].endingDate),
        image: validateImage(data[i].image),
        status: getCourseState(data[i].startingDate, data[i].endingDate),
      });
    }
  }

  return courseList;
};

export default createCourseListAdapter;
