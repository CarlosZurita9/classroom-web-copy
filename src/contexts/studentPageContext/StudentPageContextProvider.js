import React from "react";
import PropTypes from "prop-types";
import { StudentPageContext } from "./studentPageContext";

function StudentPageContextProvider({ value, children }) {
  return (
    <StudentPageContext.Provider value={value}>
      {children}
    </StudentPageContext.Provider>
  );
}

StudentPageContextProvider.propTypes = {
  value: PropTypes.shape({}).isRequired,
  children: PropTypes.node.isRequired,
};

export default StudentPageContextProvider;
