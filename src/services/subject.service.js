import axios from "axios";
import options from "../constants/subject.service.constants";
import loadAbortAxios from "../helper/loadAbortAxios";

const getSubjectsByCourseId = (courseId) => {
  const controller = loadAbortAxios();
  return {
    call: async () =>
      axios.request(
        options.GET_SUBJECTS_BY_COURSE_ID(courseId),
        { signal: controller.signal },
        controller,
      ),
  };
};

const createSubject = (subject) => {
  const controller = loadAbortAxios();
  return {
    call: async () =>
      axios.request(
        options.POST_SUBJECT(subject),
        { signal: controller.signal },
        controller,
      ),
  };
};

export { getSubjectsByCourseId, createSubject };
