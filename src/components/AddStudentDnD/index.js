import React from "react";
import { useSelector } from "react-redux";
import { Box } from "@mui/material";
import SeparatorBar from "../SeparatorBar";
import addFilterInputs from "../FilterStudentList/FilterInputs/addFilterInputs";
import FilterDnDList from "../FilterDnDList";

function AddStudentDnD() {
  const countries = useSelector((state) => state.countries.result);

  return (
    <>
      <SeparatorBar title="Add Student" />
      <Box sx={{ m: 2 }}>
        <FilterDnDList inputs={addFilterInputs(countries)} />
      </Box>
    </>
  );
}

export default AddStudentDnD;
