import React from "react";
import PropTypes from "prop-types";
import { Typography, Paper } from "@mui/material";

function DayCard({ title }) {
  return (
    <Paper elevation={2} sx={{ p: 2, width: 220, textAlign: "center" }}>
      <Typography variant="h4">{title}</Typography>
    </Paper>
  );
}

DayCard.propTypes = {
  title: PropTypes.string.isRequired,
};

export default DayCard;
