const getDaysString = (daysOfWeek) => {
  const days = [];
  if (daysOfWeek % 2 === 1) days.push("Mon");
  if (Math.trunc(daysOfWeek / 2) % 2 === 1) days.push("Tue");
  if (Math.trunc(daysOfWeek / 4) % 2 === 1) days.push("Wed");
  if (Math.trunc(daysOfWeek / 8) % 2 === 1) days.push("Thu");
  if (Math.trunc(daysOfWeek / 16) % 2 === 1) days.push("Fri");

  if (days.length === 1) return days[0];

  let daysString = "";
  for (let i = 0; i < days.length - 1; i += 1) {
    daysString += `${days[i]}, `;
  }
  return `${daysString}and ${days[days.length - 1]}`;
};

export default getDaysString;
